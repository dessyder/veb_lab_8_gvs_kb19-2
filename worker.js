onmessage = function(event) {
    let primes = findPrimes(event.data.from, event.data.to);
    postMessage(
        {type: "PrimeList", data: primes}
    );
};

function findPrimes(fromNumber, toNumber) {
    fromNumber=+fromNumber;
    toNumber=+toNumber;
    let list = [];
    for (let i=fromNumber; i<=toNumber; i++) {
        if (i>1) {
            list.push(i);
        }
    }
    let maxDiv = Math.round(Math.sqrt(toNumber));
    let primes = [];
    let result;
    for (let i=0; i<list.length; i++) {
        let failed = false;
        let t0 = performance.now();
        for (let j=2; j<=maxDiv; j++) {
            if ((list[i] != j) && (list[i] % j == 0)) {
                failed = true;
            } else if ((j==maxDiv) && (failed == false)) {
                let t1 = performance.now();
                primes.push(list[i]+"("+(t1-t0)+")");
                result=list[i]+"("+(t1-t0)+")";
                postMessage(
                    {type: "Element", data: result}
                );
            }
        }
    }
    return primes;
}