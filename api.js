let statusDisplay= document.getElementById("status");
let worker;
let searchButton= document.getElementById("searchButton");

// функція пошуку простих чисел
function doSearch() {
    searchButton.disabled = true;
    document.getElementById("primeContainer").innerText="";
    let fromNumber =document.getElementById("from").value;
    let toNumber = document.getElementById("to").value;
    worker = new Worker("worker.js");
    worker.onmessage = receivedWorkerMessage;
    worker.onerror = workerError;

    worker.postMessage(
        { from: fromNumber,
            to: toNumber
        }
    );
    statusDisplay.innerHTML = "Веб-воркер в роботі від ("+
        fromNumber + " до " + toNumber + ") ...";
}

// оголошення події при завантаженні сторінки
window.addEventListener('load',function () {
    if (localStorage.getItem('elem')!=null) {
        document.getElementById("to").value=localStorage.getItem('B');
        document.getElementById("from").value=localStorage.getItem('A');
        worker = new Worker("worker.js");
        worker.onmessage = receivedWorkerMessage;
        worker.onerror = workerError;
        let elm = localStorage.getItem('elem');
        let regex=/\d+/;
        let elem=regex.exec(elm);
        let toNumber = document.getElementById("to").value;
        worker.postMessage(
            { from: elem,
                to: toNumber
            }
        );
    }
});
let t="";

// функція отримання повідомлення зі сторони користувача
function receivedWorkerMessage(event) {
    let message = event.data;
    let primeList = "";
    let displayList = document.getElementById("primeContainer");
    if (message.type == "PrimeList") {
        let primes = message.data;
        let t = localStorage.getItem('t');
        displayList.innerHTML=t;
        for (let i=0; i<primes.length; i++) {
            primeList += primes[i];
            if (i != primes.length-1) primeList += ", ";
        }
        displayList.innerHTML += primeList;
        if (primeList.length == 0) {
            statusDisplay.innerHTML = "Відсутній результат.";
        }
        else {
            Notification.requestPermission().then(function (){
                let not=new Notification('Message',
                    {
                        'body':'Прості числа знайдені'
                    });
            });
            statusDisplay.innerHTML = "Результат готовий!"
            localStorage.clear();
        }
        searchButton.disabled = false;
    }
    else if (message.type == "Element") {
        statusDisplay.innerHTML = message.data ;
        t+=message.data;
        t+=", ";
        window.onunload=function (){
            if((worker!==null) && (statusDisplay.innerHTML !== "Результат готовий!")){

                let toNumber = document.getElementById("to").value;
                let fromNumber =document.getElementById("from").value;
                let regex=/\d+/;
                let elems=regex.exec(message.data);

                localStorage.setItem("t",t);
                localStorage.setItem("elem",elems);
                localStorage.setItem("A",fromNumber);
                localStorage.setItem("B",toNumber);
            }
        }
    }
}

function workerError(error) {
    statusDisplay.innerHTML = error.message;
}

// функція зупинки пошуку простих чисел
function cancelSearch() {
    statusDisplay.innerHTML += " -останній знайдений елемент... Потік зупинено.";
    searchButton.disabled = false;
    localStorage.clear();
    worker.terminate();
    worker = null;
}

// неактивність сторінки
document.addEventListener('visibilitychange',function (){
    if(document.visibilityState==="hidden"){
        setTimeout(mes,1000)
    }
})
function mes(){
    Notification.requestPermission().then(function () {
        let not = new Notification('Message',
            {
                'body': 'Вкладка не активна'
            });
    })
}

// google карта
let map;
document.getElementById('geolocationButton').addEventListener('click', function (){
    navigator.geolocation.getCurrentPosition(function (position){
        document.getElementById('geolocation').innerText = 'Широта: ' + position.coords.latitude + '\nДовгота: ' + position.coords.longitude;
    });
    let result = document.getElementById('geolocation');
    let myOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("mapSurface"), myOptions);
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            geolocationSuccess);
    }
    else {
        result.innerHTML = "Пошук заборонений";
    }
});
function geolocationSuccess(position) {
    var location = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
    map.setCenter(location);
    let infowindow = new google.maps.InfoWindow();
    infowindow.setContent("Ви знаходитесь десь тут.");
    infowindow.setPosition(location);
    infowindow.open(map);
}
